#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <current.h>

/* 
 * This simple default synchronization mechanism allows only creature at a time to
 * eat.   The globalCatMouseSem is used as a a lock.   We use a semaphore
 * rather than a lock so that this code will work even before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */

//static struct semaphore *globalCatMouseSem;

/* My code starts here
 *
 *
 ******************************************************************************/

// critical variable bowl_in_use, array of boolean values to indicate if a given
//   bowl is currently being eaten from

// semaphore for limiting the # of creatures we allow per "batch"

//struct thread* current_thread = curthread; // for debugging purposes
static unsigned int batch_size;
static unsigned int num_bowls; // save internal copy of # bowls
static bool *bowl_in_use;
static struct lock *bowl_lk;
static struct cv **bowls_cv;

// critical variable num_eaters & its sync variables
static unsigned int volatile num_eaters;
static unsigned int volatile num_cat_eaters;
static unsigned int volatile num_mouse_eaters;
static struct semaphore *batch_sem; // only allow batch_size creature into critical section at once
static struct lock *num_eaters_lk;

// critical variable cur_creature & its lock and CV
static char volatile cur_creature;
static struct lock *creature_lk;
static struct cv *creature_cv;

/* Change creature
 *
 * Changes the global creature context between mouse and cat concurrently
 *
 * animal will be "c" for cat or "m" for mouse
 * */

// concurrently changes the current creature, either mouse or cat
static void change_creature(const char);
static void change_creature(const char creature)
{
    unsigned int other_eaters;

    lock_acquire(creature_lk);
      if ( creature == 'm' ){
        other_eaters = num_cat_eaters;
      } else if ( creature == 'c' ){
        other_eaters = num_mouse_eaters;
      }

      // if cur_creature is default, just set it
      if (cur_creature == '?'){
        cur_creature = creature;
      }
      // while only let 1 type of creature through while there is > 0 creatures eating
      while ( cur_creature != creature && other_eaters > 0){
          cv_wait(creature_cv, creature_lk);

          DEBUG(DB_SYNCPROB, "[change_creature] Waking up from creature_cv wait \n");
          // we MUST update other_eaters, it still retains the old value
          if ( creature == 'm' ){
            other_eaters = num_cat_eaters;
          } else if ( creature == 'c' ){
            other_eaters = num_mouse_eaters;
          }
      }
      cur_creature = creature;
      KASSERT( cur_creature == creature );
    lock_release(creature_lk);
}

// acquires the given bowl number for a creature, if that bowl is in use
//   sleep in that bowl's condition variable
static void get_bowl(int);
static void get_bowl( int bowl ){
    lock_acquire( bowl_lk );

      // while the bowl we want to eat from is in use, wait
      while( bowl_in_use[bowl-1] ){
        cv_wait( bowls_cv[bowl-1], bowl_lk );
      }
      // check that bowl is not in use and then claim it
      KASSERT( bowl_in_use[bowl-1] == false );
      bowl_in_use[bowl-1] = true;

    lock_release( bowl_lk );
}

// release a given bowl, allowing another creature to pick it up
//   wakes up 1 thread in the CV of the given bowl
static void release_bowl(int);
static void release_bowl( int bowl ) {
    lock_acquire( bowl_lk );

      bowl_in_use[bowl-1] = false;
      // wake up someone waiting for the bowl
      cv_signal(bowls_cv[bowl-1], bowl_lk);

    lock_release( bowl_lk );
}

// allows the next batch of size( batch_size ) to enter the top level crit section
static void next_batch();
static void next_batch(){
    // next batch should only be allowed when there are no
    //   threads in any part of any critical section
    //KASSERT( batch_sem->sem_count == 0 );
    KASSERT( num_eaters == 0 );
    KASSERT( num_cat_eaters == 0 );
    KASSERT( num_mouse_eaters == 0 );

    for( unsigned int i = 0; i < batch_size; ++i ){
      V( batch_sem );
    }

}

/* 
 * The CatMouse simulation will call this function once before any cat or
 * mouse tries to eat.
 *
 * You can use it to initialize synchronization and other variables.
 * 
 * parameters: the number of bowls
 */
void
catmouse_sync_init(int bowls)
{
  /* replace this default implementation with your own implementation of catmouse_sync_init */

  //(void)bowls; /* keep the compiler from complaining about unused parameters */
  //globalCatMouseSem = sem_create("globalCatMouseSem",1);
  //if (globalCatMouseSem == NULL) {
  //  panic("could not create global CatMouse synchronization semaphore");
  //}

  /* my code starts here
   *
   *
   * */
  
  DEBUG(DB_SYNCPROB, "[catmouse_sync_init] START;  # bowls: %d \n", bowls);
  // initalize bowls related synchronization variables
  num_bowls = (unsigned int)bowls; // cast int bowls as unsigned int because warnings
  batch_size =  3 * num_bowls;

  bowl_in_use = kmalloc(sizeof(bool)*bowls);
  for(int i = 0; i < bowls; ++i){
    bowl_in_use[i] = false;
  }

  bowl_lk = lock_create("bowl_lk");
  if ( bowl_lk == NULL){
    panic("could not create global bowl_lk synchronization lock");
  }

  // a seprate CV for each bowl we have
  bowls_cv = (struct cv **)kmalloc( sizeof(struct cv*) * bowls );
  for(int i = 0; i < bowls; ++i){
    bowls_cv[i] = cv_create("bowls_cv");
  }

  // initialize eaters and it's related synchronization variables
  num_eaters = 0;
  num_cat_eaters = 0;
  num_mouse_eaters = 0;

  batch_sem = sem_create("batch_sem", 2*bowls);
  if (batch_sem == NULL) {
    panic("could not create global batch_sem synchronization semaphore");
  }
  KASSERT(batch_sem->sem_count == 2*bowls);

  num_eaters_lk = lock_create("num_eaters");
  if ( num_eaters_lk == NULL ){
    panic("could not create global num_eaters_lk synchronization lock");
  }

  // initialize cur_creature and associated synchronization variables
  //    default value that is neither 'c' or 'm' to start
  cur_creature = '?';

  creature_lk = lock_create("creature_lk");
  if ( creature_lk == NULL){
    panic("could not create global creature_lk synchronization lock");
  }

  creature_cv = cv_create("creature_cv");
  if( creature_cv == NULL){
    panic("could not create global creature_cv synchronization conditional variable");
  }


  DEBUG(DB_SYNCPROB, "[catmouse_sync_init] END\n");
  return;
}


/*
 * The CatMouse simulation will call this function once after all cat
 * and mouse simulations are finished.
 *
 * You can use it to clean up any synchronization and other variables.
 *
 * parameters: the number of bowls
 */
void
catmouse_sync_cleanup(int bowls)
{
  /* replace this default implementation with your own implementation of catmouse_sync_cleanup */
  //(void)bowls; /* keep the compiler from complaining about unused parameters */
  //KASSERT(globalCatMouseSem != NULL);
  //sem_destroy(globalCatMouseSem);

  DEBUG(DB_SYNCPROB, "[catmouse_sync_cleanup] START\n");
  KASSERT( creature_cv != NULL );
  cv_destroy( creature_cv );

  KASSERT( creature_lk != NULL );
  lock_destroy( creature_lk );

  KASSERT( num_eaters_lk != NULL );
  lock_destroy( num_eaters_lk );

  KASSERT( batch_sem != NULL );
  sem_destroy( batch_sem );

  for(int i = 0; i < bowls; ++i){
    KASSERT( bowls_cv[i] != NULL );
    cv_destroy( bowls_cv[i] );
  }

  kfree( bowls_cv );

  KASSERT( bowl_lk != NULL );
  lock_destroy( bowl_lk );

  kfree( bowl_in_use  );
  DEBUG(DB_SYNCPROB, "[catmouse_sync_cleanup] END\n");
  return;
}


/*
 * The CatMouse simulation will call this function each time a cat wants
 * to eat, before it eats.
 * This function should cause the calling thread (a cat simulation thread)
 * to block until it is OK for a cat to eat at the specified bowl.
 *
 * parameter: the number of the bowl at which the cat is trying to eat
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
cat_before_eating(unsigned int bowl)
{
  /* replace this default implementation with your own implementation of cat_before_eating */
  //(void)bowl;  /* keep the compiler from complaining about an unused parameter */
  //KASSERT(globalCatMouseSem != NULL);
  //P(globalCatMouseSem);

  /* My code starts here
  *
  *
  *****************************************************************************/
  // synchronization starts here
  //   only allow <= # bowls creatures to enter at one time
  DEBUG(DB_SYNCPROB, "[cat_before_eating] START\n");
  P( batch_sem );

  // increase the total # of eaters and cat_eaters
  lock_acquire( num_eaters_lk );
    ++num_cat_eaters;
    ++num_eaters;
    KASSERT( num_cat_eaters + num_mouse_eaters == num_eaters );
  lock_release( num_eaters_lk );

  // make sure the count is not messed up
  KASSERT( num_eaters <= batch_size );
  DEBUG(DB_SYNCPROB, "[#eaters] #cat %d -- #mouse: %d -- #total: %d \n", num_cat_eaters, num_mouse_eaters, num_eaters);

  // c for cat, will block mouse while eaters are cats
  change_creature('c'); 

  // check bowl that we want is not in use
  get_bowl( bowl );
  DEBUG(DB_SYNCPROB, "[cat_before_eating] END\n");
  return;
}

/*
 * The CatMouse simulation will call this function each time a cat finishes
 * eating.
 *
 * You can use this function to wake up other creatures that may have been
 * waiting to eat until this cat finished.
 *
 * parameter: the number of the bowl at which the cat is finishing eating.
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
cat_after_eating(unsigned int bowl)
{
  /* replace this default implementation with your own implementation of cat_after_eating */
  //(void)bowl;  /* keep the compiler from complaining about an unused parameter */
  //KASSERT(globalCatMouseSem != NULL);
  //V(globalCatMouseSem);

  /* My code starts herewhere
   *
   *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  DEBUG(DB_SYNCPROB, "[cat_after_eating] START\n");
  // release the bowl I am holding
  release_bowl( bowl );

  // decrement # of eaters and cat_eaters
  lock_acquire( num_eaters_lk );
    --num_cat_eaters;
    --num_eaters;
    KASSERT( num_cat_eaters + num_mouse_eaters == num_eaters );
  lock_release( num_eaters_lk );

  // when cats are done eating and we have mouse waiting to eat
  if ( num_cat_eaters == 0 && num_mouse_eaters > 0 ) {
    lock_acquire(creature_lk);
      // wake up all the mouse when cats are done eating
      cv_broadcast(creature_cv, creature_lk);
    lock_release(creature_lk);
  }

  // when this batch of creature is complete, let next batch in
  if ( num_cat_eaters == 0 && num_mouse_eaters == 0 && num_eaters == 0 ){
    next_batch();
  }
  DEBUG(DB_SYNCPROB, "[cat_after_eating] END\n");
  return;
}

/*
 * The CatMouse simulation will call this function each time a mouse wants
 * to eat, before it eats.
 * This function should cause the calling thread (a mouse simulation thread)
 * to block until it is OK for a mouse to eat at the specified bowl.
 *
 * parameter: the number of the bowl at which the mouse is trying to eat
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
mouse_before_eating(unsigned int bowl)
{
  /* replace this default implementation with your own implementation of mouse_before_eating */
  //(void)bowl;  /* keep the compiler from complaining about an unused parameter */
  //KASSERT(globalCatMouseSem != NULL);

  /* My code starts here
  *
  *
  *****************************************************************************/
  DEBUG(DB_SYNCPROB, "[mouse_before_eating] START\n");
  // synchronization starts here
  //   only allow <= # bowls creatures to enter at one time
  P( batch_sem );

  // increase the total # of eaters and mouse_eaters
  lock_acquire( num_eaters_lk );
    ++num_mouse_eaters;
    ++num_eaters;
    KASSERT( num_cat_eaters + num_mouse_eaters == num_eaters );
  lock_release( num_eaters_lk );

  // make sure the count is not messed up
  KASSERT( num_eaters <= batch_size );
  DEBUG(DB_SYNCPROB, "[#eaters] #cat %d -- #mouse: %d -- #total: %d \n", num_cat_eaters, num_mouse_eaters, num_eaters);

  // c for cat, will block mouse while eaters are cats
  change_creature('m');
  // check bowl that we want is not in use
  get_bowl( bowl );
  DEBUG(DB_SYNCPROB, "[mouse_before_eating] END\n");
  return;

}

/*
 * The CatMouse simulation will call this function each time a mouse finishes
 * eating.
 *
 * You can use this function to wake up other creatures that may have been
 * waiting to eat until this mouse finished.
 *
 * parameter: the number of the bowl at which the mouse is finishing eating.
 *             legal bowl numbers are 1..NumBowls
 *
 * return value: none
 */

void
mouse_after_eating(unsigned int bowl)
{
  /* replace this default implementation with your own implementation of mouse_after_eating */
  //(void)bowl;  /* keep the compiler from complaining about an unused parameter */
  //KASSERT(globalCatMouseSem != NULL);
  //V(globalCatMouseSem);

  /* My code starts here
   *
   *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  DEBUG(DB_SYNCPROB, "[mouse_after_eating] START\n");

  // release the bowl I am holding
  release_bowl( bowl );

  // decrement # of eaters and mouse_eaters
  lock_acquire( num_eaters_lk );
    --num_mouse_eaters;
    --num_eaters;
    KASSERT( num_cat_eaters + num_mouse_eaters == num_eaters );
  lock_release( num_eaters_lk );

  // when cats are done eating and we have mouse waiting to eat
  if ( num_mouse_eaters == 0 && num_cat_eaters > 0 ) {
    lock_acquire(creature_lk);
      // wake up all the mouse when cats are done eating
      cv_broadcast(creature_cv, creature_lk);
    lock_release(creature_lk);
  }

  // when this batch of creature is complete, let next batch in
  if ( num_cat_eaters == 0 && num_mouse_eaters == 0 && num_eaters == 0 ){
    next_batch();
  }
  DEBUG(DB_SYNCPROB, "[mouse_after_eating] END\n");
  return;
}
